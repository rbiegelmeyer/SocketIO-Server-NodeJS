var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var clients = {};

app.get('/', function(req, res) {
    res.send('server is running');
});

app.get('/test', function(req, res) {
    res.send('Rodando');
});


io.sockets.on('connection', function(client) {
    console.log('conectou');
    client.on('join', function(id) {
        console.log(id);
        client.id = id;
        clients[id] = client;
        console.log('\x1b[32m%s\x1b[0m', '\n===> Joined: ' + id);
        console.log('=======================');
        clients[id].emit('connected', 'Server Socket Roberto');
    });

    client.on('teste', function(data) {
        console.log(data);
    });

    client.on('identify', function(data) {
        //"aa:bb:cc:dd:ee:ff"
        console.log(data);
        client.id = data.mac;
        clients[client.id] = client;
        console.log('\x1b[32m%s\x1b[0m', '\n===> Joined: ' + client.id);
        console.log('=======================');
        client.emit('identified', client.id);
    });

    client.on('data', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Message');
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log('\x1b[36m%s\x1b[0m', 'TO ID:   ' + data.id);
        console.log("=======================");

        if (data.id in clients) {
            console.log('\x1b[36m%s\x1b[0m', 'Data.relay: ' + data.relay);
            clients[data.id].emit('relay', data.relay);
            console.log('\x1b[36m%s\x1b[0m', 'Message sent!');
        } else {
            console.log('\x1b[31m%s\x1b[0m', 'Message not sent!');
            console.log('\x1b[31m%s\x1b[0m', 'Invalid ID!');
            console.log('=======================');
        }
    });

    client.on('current', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Current');
        console.log('\x1b[36m%s\x1b[0m', new Date(Date.now()).toLocaleString());
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'data.I1: ' + data.I1);
    });

    client.on('potencia', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Potency');
        console.log('\x1b[36m%s\x1b[0m', new Date(Date.now()).toLocaleString());
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'data.p1: ' + data.p1);
    });

    client.on('voltage', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Voltage');
        console.log('\x1b[36m%s\x1b[0m', new Date(Date.now()).toLocaleString());
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'data.t1: ' + data.t1);
    });

    client.on('leitura', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Leitura');
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + data.id);
        console.log("=======================");

        for (var i = 0; i < Object.keys(data.ports).length; i++) {
            if (data.ports[i].type == 0) {
                console.log('Digital Porta ' + i + 1 + ': ' + data.ports[i].value);
            } else if (data.ports[i].type == 2) {
                console.log('Analogica Porta ' + i + 1 + ': ' + data.ports[i].value);
            }
        }

        console.log("=======================");
    });

    client.on('digital', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Digital');
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log("=======================");

        if ('d1' in data) { console.log(data.d1 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D1: ' + data.d1); }
        if ('d2' in data) { console.log(data.d2 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D2: ' + data.d2); }
        if ('d3' in data) { console.log(data.d3 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D3: ' + data.d3); }
        if ('d4' in data) { console.log(data.d4 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D4: ' + data.d4); }
        if ('d5' in data) { console.log(data.d5 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D5: ' + data.d5); }
        if ('d6' in data) { console.log(data.d6 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D6: ' + data.d6); }
        if ('d7' in data) { console.log(data.d7 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D7: ' + data.d7); }
        if ('d8' in data) { console.log(data.d8 ? '\x1b[32m%s\x1b[0m' : '\x1b[31m%s\x1b[0m', 'D8: ' + data.d8); }

        console.log("=======================");
    });

    client.on('analogic', function(data) {
        console.log('\x1b[36m%s\x1b[0m', '\n===> Analogic');
        console.log("=======================");
        console.log('\x1b[36m%s\x1b[0m', 'FROM ID: ' + client.id);
        console.log("=======================");

        if ('a1' in data) { console.log('\x1b[36m%s\x1b[0m', 'A1: ' + data.a1); }
        if ('a2' in data) { console.log('\x1b[36m%s\x1b[0m', 'A2: ' + data.a2); }
        if ('a3' in data) { console.log('\x1b[36m%s\x1b[0m', 'A3: ' + data.a3); }
        if ('a4' in data) { console.log('\x1b[36m%s\x1b[0m', 'A4: ' + data.a4); }

        console.log("=======================");
    });

    client.on('disconnect', function() {
        console.log('\x1b[31m%s\x1b[0m', '\n===> Disconnect: ' + client.id);
        console.log('=======================');
        delete clients[client.id];
    });
});

http.listen(3000, function() {
    console.log('listening on port 3000');
});