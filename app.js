var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
// var chart = require('chart.js')(http);
// Na pasta public é onde colocaremos o arquivo Chart.js
app.use(express.static(__dirname + '/public'));


app.get("/", function(req, res) {
    res.sendfile("view/index.html");
});

dados = { 'valor': 1 };

app.get("/2", function(req, res) {
    io.emit("dados", dados);
    res.send(dados);
});


io.sockets.on('connection', function(client) {
    client.on("dados", function(data) {
        io.emit("dados", data);
        console.log(data);
    });
});


io.on("connection", function(socket) {
    console.log("Usuário está conectádo!");
});



http.listen("3000", function() {
    console.log("Servidor on-line em http://localhost:3000 - para sair Ctrl+C.");
});