var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

io.set('heartbeat timeout', 15000);
io.set('heartbeat interval', 25000);

var clients = {};

app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.get('/', function(req, res) {
    console.log(res);
    res.send('server is running');
});


app.post('/socketio', function(req, res) {

    try {
        temp = JSON.parse(req.body);
    } catch (e) {
        temp = req.body
    }
    console.log(temp);
    // var msg = '';
    // for (var i = 0; i < temp.rl.length; i++) {
    //     msg += temp.rl[i]
    // }
    var msg = { "rl": temp.rl };
    console.log(msg);

    try {
        clients[temp.id].emit('relay', msg);
    } catch (e) {
        console.log(e);
    }
    res.send('server is running ');
});

io.sockets.on('connection', function(client) {

    client.on("join", function(data) {
        client.id = data.id;
        clients[data.id] = client;
        msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' joined')
        console.log('\x1b[32m%s\x1b[0m', msg);
    });

    client.on("png", function(data) {
        if (!(client.id in clients)) {
            console.log(client.id);
            clients[client.id] = client;
            msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' rejoined *');
            console.log('\x1b[32m%s\x1b[0m', msg);
        }
        msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' ' + data.message)
        console.log('\x1b[33m%s\x1b[0m', msg);
    });

    client.on("ret", function(data) {
        if (!(client.id in clients)) {
            console.log(client.id);
            clients[client.id] = client;
            msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' rejoined *');
            console.log('\x1b[32m%s\x1b[0m', msg);
        }
        msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' Sucess: ' + data.sucess)
        console.log('\x1b[29m%s\x1b[0m', msg);
    });

    client.on("disconnect", function() {
        msg = (new Date(Date.now()).toLocaleString() + ' ' + client.id + ' disconnected')
        console.log('\x1b[31m%s\x1b[0m', msg);
        delete clients[client.id];
    });

});

http.listen(3000, function() {
    console.log('listening on port 3000');
});